$(function() {
	if(window.location.pathname==='/penjualan'){
		$("body").addClass('sidebar-collapse');
		$(".content-header").remove();
		$(".content").addClass("pt-2");
	}

	/**
	 * Fungsi untuk membuat format ribuan
	 */
	 function rupiah(nominal) {
		let number_string = nominal.toString(), // convert nominal ke string
			sisa = number_string.length % 3, // cek jumlah digit bukan kelipatan 3
			rupiah = number_string.substr(0, sisa),
			ribuan = number_string.substr(sisa).match(/\d{3}/g);
			if (ribuan) {
				let separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
		return rupiah;
	}

	/**
	 * Menampilkan detail isi keranjang
	 */
	function detailKeranjang(){
		$.ajax({
			url: "/penjualan/keranjang",
			dataType : "json",
			success: function (response) {
				$("#invoice").text(response.invoice); // menampilkan no invoice
				$("#tampilkan_total").text(rupiah(response.sub_total)); // menampilkan total harga
				$("#sub_total").val(response.sub_total); // isi value sub_total
				$("#total_akhir").val(response.sub_total); // isi value total_akhir
				// menampilkan detail keranjang
				let keranjang;
				if (response.keranjang.length === 0) {
					keranjang = `<tr><td colspan="7" class="text-center">Keranjang belanja kosong</td></tr>`;

					$("#diskon").prop("disabled", true);
					$("#tunai").prop("disabled", true);
					$("#batal").prop("disabled", true);
					$("#bayar").prop("disabled", true);
					$("#catatan").prop("disabled", true);
				} else {
					$("#diskon").prop("disabled", false);
					$("#tunai").prop("disabled", false);
					$("#batal").prop("disabled", false);
					$("#bayar").prop("disabled", false);
					$("#tunai").val('');
					$("#catatan").prop("disabled", false);

					$.each(response.keranjang, function (i, data) {
						keranjang += `<tr>
						<td>`+data.barcode+`</td>
						<td>`+data.nama+`</td>
						<td>`+data.harga+`</td>
						<td>`+data.jumlah+`</td>
						<td>`+data.diskon+`</td>
						<td>`+data.total+`</td>
						<td>
							<button class="btn btn-success btn-sm" id="edit-item" data-toggle="modal" data-target="#modal-item-edit" data-id="`+data.id+`" data-barcode="`+data.barcode+`" data-item="`+data.nama+`" data-harga="`+data.harga+`" data-jumlah="`+data.jumlah+`" data-diskon="`+data.diskon+`" data-subtotal="`+data.total+`" data-stok="`+data.stok+`"><i class="fa fa-edit"></i></button>
							<button class="btn btn-danger btn-sm" id="hapus-item" data-id="`+data.id+`"><i class="fa fa-trash"></i></button>
						</td>
						</tr>`
					});
				}
				$("tbody").html(keranjang);
			}
		});
	}
	detailKeranjang() // pertama halaman dibuka load detail keranjang
	
	// Cari item berdasarkan barcode
	$( "#barcode" ).autocomplete({
		source: "/item/barcode",
		autoFocus: true,
		select: function( event, ui ) {
			$("#barcode").prop("disabled", true);
			$.ajax({
				url: "/item/detail",
				type: 'post',
				data: {
					[$("#token").attr('name')]: $("#token").val(),
					"barcode":ui.item.value
				},
				success: function (response) {
					$("#iditem").val(response.iditem)
					$("#barcode").val(response.barcode)
					$("#nama").val(response.item)
					$("#harga").val(response.harga)
					$("#stok").val(response.stok)
					$("#jumlah").prop("disabled", false).focus()
				}
			});
		}
	});

	// tambahkan item ke keranjang
	$("#jumlah").on("keypress", function(e){
		let iditem = $("#iditem").val()
		let barcode = $("#barcode").val()
		let nama = $("#nama").val()
		let harga = $("#harga").val()
		let stok = $("#stok").val()
		let jumlah = $("#jumlah").val()

		if (e.keyCode === 13) { // kode keyboard enter
			// jika jumlah melebihi stok
			if (parseInt(jumlah) > parseInt(stok)) {
				$("#jumlah").val(stok)
				toastr.error("Stok yang tersedia hanya " + stok, '', {timeOut: 500});
			} else if(jumlah == '' || jumlah < 1){
				$("#jumlah").val(1)
				toastr.error("Jumlah minimal 1", '', {timeOut: 500});
			} else {
				// sip, tambahkan item ke keranjang
				$.ajax({
					url: "/penjualan/tambah",
					method: "post",
					data: {
						[$("#token").attr('name')]: $("#token").val(),
						iditem: iditem,
						barcode: barcode,
						nama: nama,
						harga: harga,
						jumlah: jumlah,
						stok: stok
					},
					success: function(response) {
						if (response.status) {
							detailKeranjang()
							$("#jumlah").val('').prop("disabled", true)
							$("#barcode").val('').prop("disabled", false).focus()
							toastr.success(response.pesan, 'Sukses',{timeOut: 100});
						} else {
							toastr.error(response.pesan);
						}
					}
				})	
			}
		}
	})

	// hapus item di keranjang
	$(".content").on("click", "#hapus-item", function() {
		Swal.fire({
			title: 'Yakin ingin menghapus item ini?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya, hapus!',
			cancelButtonText: 'Batal'
		}).then(result => {
			if (result.isConfirmed) {
				$.ajax({
					url: "/penjualan/hapus",
					type: 'post',
					data: {
						[$("#token").attr('name')]: $("#token").val(),
						iditem: $(this).data('id')
					},
					success: function (response) {
						if (response.status) {
							detailKeranjang()
							$("#tunai").val(0)
							toastr.success(response.pesan, 'Sukses',{timeOut: 100});
						} else {
							toastr.error(response.pesan, 'Error',{timeOut: 100})
						}
					}
				})
			}
		})
	})

	// modal form edit item keranjang
	$(".content").on("click", "#edit-item", function() {
		// mengambil data dari tombol select, input ke tiap-tiap elemet
		$('#item_id').val($(this).data('id'))
		$('#item_barcode').val($(this).data('barcode'))
		$('#item_nama').val($(this).data('item'))
		$('#item_harga').val($(this).data('harga'))
		$('#item_jumlah').val($(this).data('jumlah')).prop("max", $(this).data('stok'))
		$('#item_stok').val($(this).data('stok'))
		$('#item_diskon').val($(this).data('diskon'))
		$('#harga_sebelum_diskon').val($(this).data('subtotal'))
		$('#harga_setelah_diskon').val($(this).data('subtotal'))
	})

	// update isi keranjang
	$(".wrapper").on("click", "#edit-keranjang", function() {
		$.ajax({
			url: "/penjualan/ubah",
			type: "post",
			dataType: "json",
			data: $("form").serialize(),
			success: function(response) {
				$('#modal-item-edit').modal('hide')
				$("#barcode").focus()
				detailKeranjang()
				toastr.success(response.pesan, 'Sukses',{timeOut: 100});
			}
		})
	})

	/**
	 * Form modal update live
	 */
	function modal_edit_item() {
		let jumlah = $("#item_jumlah").val();
		let harga = $("#item_harga").val()
		let stok = $("#item_stok").val()
		let item_diskon = $("#item_diskon").val()

		if (parseInt(jumlah) > parseInt(stok)) {
			toastr.error("Jumlah maksimal " + stok, '', {timeOut: 100});
			$("#item_jumlah").val(stok);
			jumlah = stok
		} else if(jumlah == '' || jumlah < 1){
			toastr.error("Jumlah minimal 1", '', {timeOut: 100});
			$("#item_jumlah").val(1);
			jumlah = 1
		}

		harga_sebelum_diskon = (jumlah * harga)
		$("#harga_sebelum_diskon").val(harga_sebelum_diskon)
		if(item_diskon == '' || item_diskon == 0) {
			$("#item_diskon").val(0)
			$("#harga_setelah_diskon").val(harga_sebelum_diskon)
		} else {
			hasil_diskon = (item_diskon / 100 * harga_sebelum_diskon)
			$("#harga_setelah_diskon").val(harga_sebelum_diskon - hasil_diskon)
		}
	}

	/**
	 * Hitung kalkulasi diskon, total belanja dan kembalian
	 */
	function kalkulasi() {
		let sub_total = $("#sub_total").val(),
			diskon_akhir = ($("#diskon").val() / 100 * sub_total),
			total_akhir = (sub_total - diskon_akhir),
			tunai = $("#tunai").val().replaceAll('.', ''),
			kembalian = tunai - total_akhir > 0 ? rupiah(tunai - total_akhir) : tunai - total_akhir;

		$("#total_akhir").val(total_akhir)
		$("#tampilkan_total").text(rupiah(total_akhir))
		tunai != 0 ? $('#kembalian').val(kembalian) : $('#kembalian').val(0)
	}
	
	// jika kolom diskon dan tunai di edit load kalkulasi
	$(".wrapper").on('keyup mouseup', '#diskon, #tunai', function(e) {
		kalkulasi()
	})
	// jika kolom jumlah dan diskon di edit update isi total otomatis
	$(".wrapper").on('keyup mouseup', '#item_jumlah, #item_diskon', function() {
		modal_edit_item()
	})

	// batalkan pembayaran 
	$(".wrapper").on('click', '#batal', function() {
		Swal.fire({
			title: 'Yakin ingin membatalkan transaksi?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya, batal!',
			cancelButtonText: 'Tidak',
		}).then(result => {
			if (result.isConfirmed) {
				$.ajax({
					url: "/penjualan/hapus",
					success: function (response) {
						detailKeranjang()
						$("#pelanggan").val('')
						$("#diskon").val(0)
						$("#tunai").val(0)
						$("#kembalian").val(0)
						$("#barcode").focus()
						toastr.success(response.pesan, "", {timeOut: 500});
					}
				})
			}
		})
	})

	// proses pembayaran
	$(".wrapper").on('click', '#bayar', function() {
		let id_pelanggan = $("#pelanggan").val()
		let subtotal = $("#sub_total").val()
		let diskon = $("#diskon").val()
		let total_akhir = $("#total_akhir").val()
		let tunai = $("#tunai").val()
		let kembalian = $("#kembalian").val()
		let catatan = $("#catatan").val()
		let tanggal = $("#tanggal").val()

		if (tunai < 1){
			toastr.error('Jumlah uang tunai belum diinput :(', '', {timeOut: 500});
			$("#tunai").focus()
		} else if (id_pelanggan == null){
			toastr.error('Pelanggan belum di tentukan :(', '', {timeOut: 500});
			$("#pelanggan").focus()
		} else {
			// semua sudah oke
			Swal.fire({
				title: 'Yakin proses transaksi sudah benar?',
				icon: 'question',
				showCancelButton: true,
				confirmButtonText: 'Ya yakin :)',
				cancelButtonText: 'Batal',
			}).then(result => {
				if (result.isConfirmed) {
					$.ajax({
						url: "/penjualan/bayar",
						type: "post",
						dataType: "json",
						data: {
							[$("#token").attr('name')]: $("#token").val(),
							id_pelanggan: id_pelanggan,
							subtotal: subtotal,
							diskon: diskon,
							total_akhir: total_akhir,
							tunai: tunai,
							kembalian: kembalian,
							catatan: catatan,
							tanggal: tanggal
						},
						success: function (response) {
							if (response.status) {
								detailKeranjang()
								$("#catatan").val('')
								$("#pelanggan").val('')
								$("#kembalian").val(0)
								auto_numeric.set(0) // library dari AutoNumeric.js
								toastr.success(response.pesan, "", {
									timeOut: 500,
									onHidden: function(){
										$("#barcode").focus()
										window.open('/penjualan/cetak/' + response.idpenjualan)
									}
								});
							} else {
								toastr.error(response.pesan)
							}
						}
					})
				}
			})
		}
	})
})