<?php

if (!function_exists('isLogin')) {
    /**
     * Mengecek sudah login atau belum
     * @return redirect ke halaman dashboard
     */
    function isLogin()
    {
        return session()->has('login');
    }
}

if (! function_exists('user')) {
    /**
     * Menampilkan data user yang sedang login
     * @return object $data
     */
    function user()
    {
        $service = service('user');
        $user = $service->getUser(session('id'));
        unset($user->password, $user->token, $user->status, $user->updated_at, $user->deleted_at);
        return $user;
    }
}
