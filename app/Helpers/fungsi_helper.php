<?php

if (!function_exists('rupiah')) {
    // format rupiah indonesia
    function rupiah($nominal)
    {
        return number_format($nominal, 0, ',', '.');
    }
}

if (!function_exists('buat_password')) {
    /**
     * Otomatis menghash kata sandi
     * @link https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence
     * @param string $password
     */
    function buat_password(string $password)
    {
        return password_hash(
            base64_encode(hash('sha384', $password, true)),
            PASSWORD_DEFAULT
        );
    }
}

if (!function_exists('verifikasi_password')) {
    /**
     * Otomatis memverifikasi password yang sudah di hash.
     * @link https://paragonie.com/blog/2015/04/secure-authentication-php-with-long-term-persistence
     *
     * @return bool
     */
    function verifikasi_password(string $password, string $hash)
    {
        return password_verify(base64_encode(hash('sha384', $password, true)), $hash);
    }
}