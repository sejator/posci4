<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ubah Password</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?= base_url('plugins/fontawesome-free/css/all.min.css') ?>">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?= base_url('plugins/toastr/toastr.min.css') ?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('dist/css/adminlte.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('css/style.css') ?>">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <b class="text-muted">Ubah Password</b>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Silahkan ganti password anda.</p>
                <?= form_open('auth/ganti-password'); ?>
                    <input type="hidden" name="kode" value="<?= esc($token) ?>">
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password Baru" autocomplete="off">
                        <div class="input-group-append show-password">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash"></span>
                            </div>
                        </div>
                        <small class="invalid-feedback"></small>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" id="konfirmasi_password" name="konfirmasi_password" placeholder="Konfirmasi Password" autocomplete="off">
                        <div class="input-group-append show-password">
                            <div class="input-group-text">
                                <span class="fas fa-eye-slash"></span>
                            </div>
                        </div>
                        <small class="invalid-feedback"></small>
                    </div>
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary btn-block" id="ubah">Ubah Password</i></button>
                    </div>
                <?= form_close(); ?>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
    <!-- jQuery -->
    <script src="<?= base_url('plugins/jquery/jquery.min.js') ?>"></script>
    <!-- Toastr -->
    <script src="<?= base_url('plugins/toastr/toastr.min.js') ?>"></script>
    <script src="<?= base_url('js/script.js') ?>"></script>
    <script src="<?= base_url('js/auth.js') ?>"></script>
</body>

</html>