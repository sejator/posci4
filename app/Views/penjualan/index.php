<?= $this->extend('layout/template'); ?>
<?= $this->section('content'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group row d-none">
                        <label for="tanggal" class="col-sm-3 col-form-label">Tanggal</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" name="tanggal" id="tanggal" value="<?= date('Y-m-d') ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="user" class="col-sm-3 col-form-label">Kasir</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="user" id="user" readonly value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pelanggan" class="col-sm-3 col-form-label">Pelanggan</label>
                        <div class="col-sm-9">
                            <select name="pelanggan" id="pelanggan" class="form-control">
                                <?php foreach (esc($pelanggan) as $data) : ?>
                                    <option value="<?= esc($data->id) ?>"><?= esc($data->pelanggan); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="barcode" class="col-sm-3 col-form-label">Barcode</label>
                        <div class="col-sm-9">
                            <div class="input-group mb-1">
                                <input type="hidden" id="iditem">
                                <input type="hidden" id="nama">
                                <input type="hidden" id="harga">
                                <input type="hidden" id="stok">
                                <input type="text" class="form-control" id="barcode" name="barcode" autofocus autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-sm-3 col-form-label">Jumlah</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" name="jumlah" id="jumlah" autocomplete="off" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="text-right">
                        <h4>Invoice : <span class="text-bold" id="invoice"></span></h4>
                        <h1><span class="text-bold" id="tampilkan_total">0</span></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary card-outline">
                <div class="p-1 table-responsive">
                    <table class="table table-bordered table-striped" id="tabel-keranjang" width="100%">
                        <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Nama Item</th>
                                <th>Harga</th>
                                <th>Jumlah</th>
                                <th style="width: 150px;">Diskon item (%)</th>
                                <th>Total</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- .row -->
    <div class="row">
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="sub_total" class="col-sm-5 col-form-label">Sub Total</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control text-right" name="sub_total" id="sub_total" readonly value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="diskon" class="col-sm-5 col-form-label">Diskon Total(%)</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control text-right" name="diskon" id="diskon" autocomplete="off" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="total_akhir" class="col-sm-5 col-form-label">Total Akhir</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control text-right" name="total_akhir" id="total_akhir" readonly value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .col-md-3 -->
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="tunai" class="col-sm-5 col-form-label">Tunai</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control text-right" name="tunai" id="tunai" value="0">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kembalian" class="col-sm-5 col-form-label">Kembalian</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control text-right" name="kembalian" id="kembalian" readonly value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .col-md-3 -->
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="form-group">
                        <label for="catatan">Catatan</label>
                        <textarea class="form-control" name="catatan" id="catatan" rows="3"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- .col-md-3 -->
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <p><button class="btn btn-warning" id="batal"><i class="fa fa-refresh"></i> Batal</button></p>
                    <p><button class="btn btn-success" id="bayar"><i class="fa fa-paper-plane"></i> Proses Pembayaran</button></p>
                </div>
            </div>
        </div>
        <!-- .col-md-3 -->
    </div>
    <!-- .row -->
</div>

<!-- modal edit item produk -->
<div class="modal fade show" id="modal-item-edit" aria-modal="true">
    <div class="modal-dialog">
        <?= form_open('', ['csrf_id' => 'token']); ?>
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Item</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="item_id" name="item_id">
                <input type="hidden" id="item_stok" name="item_stok">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for=""> Barcode</label>
                        <input type="text" id="item_barcode" name="item_barcode" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">Item Produk</label>
                        <input type="text" id="item_nama" name="item_nama" class="form-control" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="item_harga">Harga</label>
                        <input type="text" id="item_harga" name="item_harga" class="form-control" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="item_jumlah">Jumlah</label>
                        <input type="number" id="item_jumlah" name="item_jumlah" class="form-control" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label for="harga_sebelum_diskon">Total sebelum Diskon</label>
                    <input type="text" id="harga_sebelum_diskon" name="harga_sebelum_diskon" class="form-control" readonly>
                </div>
                <div class="form-group">
                    <label for="item_diskon">Diskon Item (%)</label>
                    <input type="number" id="item_diskon" name="item_diskon" class="form-control" min="0">
                </div>
                <div class="form-group">
                    <label for="harga_setelah_diskon">Total setelah Diskon</label>
                    <input type="text" id="harga_setelah_diskon" name="harga_setelah_diskon" class="form-control" min="0" readonly>
                </div>
            </div>
            <div class="form-group">
                <div class="float-right mr-3">
                    <button type="button" class="btn btn-success" id="edit-keranjang"><i class="fa fa-paper-plane"></i> Simpan</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
        <?= form_close(); ?>
    </div>
    <!-- /.modal-dialog -->
</div>
<script src="<?= base_url('js/penjualan.js') ?>"></script>
<script src="<?= base_url('plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<script src="<?= base_url('plugins/autoNumeric.min.js') ?>"></script>
<script>
    let auto_numeric = new AutoNumeric('#tunai',{ decimalCharacter: ",", decimalPlaces: 0, digitGroupSeparator: ".",});
</script>
<?= $this->endSection(); ?>