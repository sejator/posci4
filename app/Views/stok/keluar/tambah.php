<?php $this->extend('layout/template');
$this->section('content');
?>

<div class="container-fluid">
    <div class="card shadow mb-4">
        <?= form_open('', ['csrf_id' => 'token']); ?>
            <div class="col-md-8">
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="tanggal">Tanggal</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?= date('Y-m-d') ?>">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="hidden" id="iditem" name="iditem">
                        <input type="hidden" name="tipe" value="keluar">
                        <label for="barcode" class="col-sm-3 col-form-label">Barcode</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="barcode" name="barcode" autofocus>
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="item" class="col-sm-3 col-form-label">Nama Item</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="item" name="item" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="unit" class="col-sm-3 col-form-label">Item Unit</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="unit" name="unit" disabled value="-">
                        </div>
                        <label for="stok" class="col-sm-2 col-form-label">Stok</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="stok" name="stok" disabled value="-">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="pemasok" class="col-sm-3 col-form-label">Pemasok</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="pemasok" id="pemasok">
                                <option value="">- Pilih Pemasok -</option>
                                <?php foreach(esc($pemasok) as $data) : ?>
                                    <option value="<?= esc($data->id)?>"><?= esc($data->pemasok); ?></option>
                                <?php endforeach; ?>
                            </select>
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jumlah" class="col-sm-3 col-form-label">Jumlah</label>
                        <div class="col-sm-8">
                            <input type="number" class="form-control" id="jumlah" name="jumlah">
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="keterangan" class="col-sm-3 col-form-label">Keterangan</label>
                        <div class="col-sm-8">
                            <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                            <small class="invalid-feedback"></small>
                        </div>
                    </div>
                    <div class="form-group float-right">
                        <button type="reset" class="btn btn-danger">Reset</button>
                        <button type="submit" id="simpan" class="btn btn-primary">Simpan</button>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script src="<?= base_url('plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<script>
    $(document).ready(function() {
        // Cari item berdasarkan barcode
        $( "#barcode" ).autocomplete({
            source: "/item/barcode",
            autoFocus: true,
            select: function( event, ui ) {
                $.ajax({
                    url: "/item/detail",
                    type: 'post',
                    data: {
                        [$("#token").attr('name')]: $("#token").val(),
                        "barcode":ui.item.value
                    },
                    success: function (response) {
                        $("#iditem").val(response.iditem)
                        $("#barcode").val(response.barcode)
                        $("#item").val(response.item)
                        $("#unit").val(response.unit)
                        $("#stok").val(response.stok)
                        $("#pemasok").focus()
                    }
                });
            }
        });

        $("#simpan").on("click", function(e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: "/transaksi/proses",
                dataType: "json",
                data: $("form").serialize(),
                success: function(response) {
                    responValidasi(['kurang'], ['tanggal', 'barcode', 'pemasok', 'jumlah', 'keterangan'], response);
                }
            });
        })
    })
</script>

<?php $this->endSection(); ?>