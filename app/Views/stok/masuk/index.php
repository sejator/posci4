<?php $this->extend('layout/template');
$this->section('content');
?>

<div class="container-fluid">
    <a href="<?= base_url('stok/masuk/tambah')?>" class="btn btn-primary mb-1"><i class="fas fa-plus"></i> Tambah Stok Masuk</a>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="tabel-stok" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Barcode</th>
                            <th>Item</th>
                            <th>Jumlah</th>
                            <th>Tanggal</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal detail -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Stok Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
               <div id="detail"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        const table = $("#tabel-stok").DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: "/transaksi/stok",
                data: function (d) {
                    d.tipe = 'masuk'
                }
            },
            lengthMenu: [
                [5, 10],
                [5, 10]
            ],
            columns: [
                {
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'barcode'},
                { data: 'item'},
                { data: 'jumlah'},
                { data: 'tanggal'},
                {
                    render: function(data, type, row) {
                        let html = '<button class="btn btn-primary btn-sm mr-1 detail" data-barcode="' + row.barcode + '" data-item="' + row.item + '" data-jumlah="' + row.jumlah + '" data-pemasok="' + row.pemasok + '" data-keterangan="' + row.keterangan + '" data-tanggal="' + row.tanggal + '"><i class="fas fa-eye"></i></button>';
                        html += '<button class="btn btn-danger btn-sm hapus" data-id="' + row.id + '"><i class="fa fa-trash"></i></button>'
                        return html;
                    }
                }
            ],
            columnDefs: [{
                targets: 0,
                width: "5%",
            }, {
                targets: [0, 3, -1],
                orderable: false
            }]
        })
        $(".content").on("click", ".detail", function() {
            $("#detailModal").modal("show");
            let konten = ` <table class="table table-bordered">
                    <tr>
                        <th>Barcode</th>
                        <td>`+$(this).data("barcode")+`</td>
                    </tr>
                    <tr>
                        <th>Item Produk</th>
                        <td>`+$(this).data("item")+`</td>
                    </tr>
                    <tr>
                        <th>Jumlah</th>
                        <td>`+$(this).data("jumlah")+`</td>
                    </tr>
                    <tr>
                        <th>Pemasok</th>
                        <td>`+$(this).data("pemasok")+`</td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <td>`+$(this).data("keterangan")+`</td>
                    </tr>
                    <tr>
                        <th>Tanggal</th>
                        <td>`+$(this).data("tanggal")+`</td>
                    </tr>
                </table>`
            $("#detail").html(konten)
        })

        $(".content").on("click", ".hapus", function() {
            Swal.fire({
                title: 'Yakin ingin menghapus data ini?',
                icon: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus',
                cancelButtonText: 'Batal!'
            }).then(result => {
                if (result.isConfirmed) {
                    // ajax hapus data 
                    $.ajax({
                        url: "/transaksi/hapus",
                        data: {
                            id: $(this).data('id'),
                            tipe: 'masuk'
                        },
                        success: function(response) {
                            if(response.status){
                                table.ajax.reload()
                                toastr.success(response.pesan)
                            } else {
                                toastr.error(response.pesan)
                            }
                        }
                    });
                }
            })
        })
    })
</script>

<?php $this->endSection(); ?>