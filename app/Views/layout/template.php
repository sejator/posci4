<!DOCTYPE html>
<html lang="en">
<?= $this->include('layout/header'); ?>

<!-- <body class="hold-transition sidebar-mini layout-navbar-fixed layout-fixed <?php //uriSegment(1) == 'penjualan' ? 'sidebar-collapse' : ''; 
                                                                                ?>"> -->

<body class="hold-transition sidebar-mini layout-navbar-fixed layout-fixed">
    <div class="preloader">
        <div class="loading">
            <div class="spinner-grow text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <div class="wrapper" data-url="<?= base_url(); ?>">
        <!-- Navbar -->
        <?= $this->include('layout/navbar'); ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?= $this->include('layout/sidebar'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0"><?= esc($title); ?></h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?= base_url('dashboard') ?>">Home</a></li>
                                <li class="breadcrumb-item active"><?= ucfirst(uri_string()) ?></li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <!-- jQuery -->
            <script src="<?= base_url('plugins/jquery/jquery.min.js') ?>"></script>
            <div class="content">
                <?= $this->renderSection('content'); ?>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="float-right d-none d-sm-inline">
                Version : Beta
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; <?= date('Y') ?> <a href="<?= base_url() ?>">Sistem Penjualan</a>.</strong> All rights reserved.
        </footer>
    </div>
    <!-- ./wrapper -->
    <?= $this->include('layout/js'); ?>
</body>

</html>